export interface SimplePriceModel {
  id: string;
  price: number;
}