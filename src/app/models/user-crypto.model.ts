//user crypto data model
export interface UserCryptoModel {
  amount: number;
  id: string;
  name: string;
  symbol: string;
  thumb: string;
  large: string;
  label: string;
}