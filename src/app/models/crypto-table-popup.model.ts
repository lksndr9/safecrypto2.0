//Data for Popup table show
export interface CryptoTablePopupModel {
  id: string;
  name: string;
  logoUrl: string;
  price: number;
  amount: number;
  yourValue: number;
}