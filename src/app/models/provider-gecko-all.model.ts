import { GeckoCryptoModel } from "./gecko-crypto.model";

//provider crypto currency metadata
export interface ProviderGeckoAllModel {
  coins: GeckoCryptoModel[];
  exchanges: any[];
  icos: any[];
  categories: any[];
}