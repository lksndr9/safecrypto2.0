//provider crypto currency data
export interface ProviderCryptoModel {
  id: string;
  name: string;
  thumb: string;
  price: number;
}