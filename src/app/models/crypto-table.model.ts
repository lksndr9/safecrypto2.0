//Data for Options table show
export interface CryptoTableModel {
  id: string;
  name: string;
  label: string;
  thumb: string;
  amount: number;
}