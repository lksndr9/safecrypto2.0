//provider crypto currency data
export interface GeckoCryptoModel {
  id: string;
  name: string;
  symbol: string;
  thumb: string;
  large: string;
}