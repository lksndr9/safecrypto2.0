import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PopupComponent } from './components/popup/popup.component';
import { OptionsComponent } from './components/options/options.component';
import { StorageInterface } from './components/interfaces/storage.interface';
import { LocalStorageService } from './services/local-storage.service';
import { CryptoDataInterface } from './components/interfaces/crypto-data.interface';
import { HttpResponseMapperProvider } from './interceptors/http-response-mapper-interceptor';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HelpComponent } from './components/help/help.component';
import { CoinGeckoService } from './services/coingecko.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    AppComponent,
    PopupComponent,
    OptionsComponent,
    HelpComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule
  ],
  providers: [
    { provide: StorageInterface, useClass: LocalStorageService },
    { provide: CryptoDataInterface, useClass: CoinGeckoService },
    HttpResponseMapperProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
