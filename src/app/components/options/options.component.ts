import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserCryptoModel } from 'src/app/models/user-crypto.model';
import { CryptoDataInterface } from 'src/app/components/interfaces/crypto-data.interface';
import { StorageInterface } from 'src/app/components/interfaces/storage.interface';
import { GeckoCryptoModel } from '../../models/gecko-crypto.model';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class OptionsComponent implements OnInit {
  

  // List of user crypto currencies
  userCrypto: UserCryptoModel[] = [];
  // List of provider data for user crypto currencies
  providerCryptoData: GeckoCryptoModel[] = [];
  // Row selected
  selectedRow: number | undefined;
  // Edit mode on/off
  editCryptoOn: boolean = false;
  addNewCurrencyFG: FormGroup;
  // Flag for validity of input
  resetCryptoIdName: boolean = false;
  
  constructor(
    private cryptoDataInterface: CryptoDataInterface, 
    private formBuilder: FormBuilder,
    private storageService: StorageInterface,
    ) {

      // Form group creation
      this.addNewCurrencyFG = this.formBuilder.group({
        cryptoIdName: [null, {validators: [Validators.required], updateOn: 'change'}],
        cryptoAmount: [null, {validators: [Validators.min(0)], updateOn: 'submit'}],
        cryptoLabel: [null, {validators: [Validators.maxLength(15)], updateOn: 'submit'}]
      })
    }
    
    // Form control getters
    get cryptoIdName(): FormControl { return this.addNewCurrencyFG.get('cryptoIdName') as FormControl; }
    get cryptoAmount(): FormControl { return this.addNewCurrencyFG.get('cryptoAmount') as FormControl; }
    get cryptoLabel(): FormControl { return this.addNewCurrencyFG.get('cryptoLabel') as FormControl; }

  // Form invalid metods
  cryptoIdNameInvalid(): boolean { return this.cryptoIdName.invalid && this.cryptoIdName.touched; }
  cryptoAmountInvalid(): boolean { return this.cryptoAmount.invalid && this.cryptoAmount.touched; }
  
  submit(): void {
    this.cryptoIdName.markAsDirty();
    this.cryptoAmount.markAsDirty();
    this.cryptoLabel.markAsDirty();
    
    if (!this.addNewCurrencyFG.valid) return;
    
    const userCryptoInput: GeckoCryptoModel | undefined = this.getValidCryptoObject();
    if (userCryptoInput === undefined) return;
    
    let amount:number;
    if (this.cryptoAmount.value === null)  amount = 0
      else amount = this.cryptoAmount.value;

    let label: string;
    if (this.cryptoLabel.value === null)  label = ''
    else label = this.cryptoLabel.value;

    // Temporary object for userCrypto array
    let cryptoObject: UserCryptoModel = { 
      id: userCryptoInput.id,
      name: userCryptoInput.name,
      thumb: userCryptoInput.thumb,
      symbol: userCryptoInput.symbol,
      large: userCryptoInput.large,
      amount: amount,
      label: label
    };
    
    // Adding to userCrypto array
    if (this.editCryptoOn && this.selectedRow !== undefined) {
      this.userCrypto[this.selectedRow] = cryptoObject;
      this.editCryptoOn = false;
    } else {
      this.userCrypto.push(cryptoObject);
    }
    
    // Reset inputs
    this.storageService.saveToStorage(this.userCrypto);
    this.deselectAll();
  }
  
  ngOnInit(): void {
    this.userCrypto = this.storageService.loadFromStorage(); 
 }


  getInputData(cryptoName: string): void {

    // Crypto ID must have at least 3 characters for lower network data transfer
    if (cryptoName.length > 2){
      this.cryptoDataInterface.getMetadata(cryptoName).subscribe (
        data => {
          this.providerCryptoData = data.coins;
          this.processInputData();
        },
        () => {
          setTimeout( () => this.getInputData(''), 2000);
        }
      )
    } else return;
  }

  // Creating options for input datalist
  processInputData(): void {
    let valueCryptoIdName: string = '';
    this.providerCryptoData.forEach(element => {
      valueCryptoIdName += '<option value="' + element.symbol  +'">';
    });
    const dataListElement = document.getElementById('currencies-datalist') as HTMLElement;
    dataListElement.innerHTML = valueCryptoIdName;
  }

  // Test validity of input id and get metadata object
  getValidCryptoObject(): GeckoCryptoModel | undefined {
    const controlValue: string = this.cryptoIdName.value.toUpperCase().trim();
    const elementFound: GeckoCryptoModel | undefined = this.providerCryptoData.find(element => (
      (controlValue === element.symbol.toUpperCase()) ||
      (controlValue === element.id.toUpperCase()) ||
      (controlValue === element.symbol.toUpperCase() + ' ' + element.id.toUpperCase()) ||
      (controlValue === element.id.toUpperCase() + ' ' + element.symbol.toUpperCase())
      ));
    if (elementFound === undefined) {
      this.cryptoIdName.setErrors({testCryptoName: true});
      this.resetCryptoIdName = true;
    } 
    this.cryptoIdName.reset();
    return elementFound;
  }

  selectRow(index: number) {
    if (index !== this.selectedRow) {
      this.deselectAll();
      this.selectedRow = index;
    } 
    else this.deselectAll();
  }

  deselectAll() {
    this.selectedRow = undefined;
    this.editCryptoOn = false;
    this.addNewCurrencyFG.reset();
  }
  // Controls of table selection
  upButton(){
    if (this.selectedRow === undefined || this.selectedRow === 0) return;
    const temp = this.userCrypto[this.selectedRow];
    this.userCrypto[this.selectedRow] = this.userCrypto[this.selectedRow - 1];
    this.userCrypto[this.selectedRow - 1] = temp;
    this.selectedRow = this.selectedRow - 1;
    this.storageService.saveToStorage(this.userCrypto);
  }

  downButton(){
    if (this.selectedRow === undefined || this.selectedRow === this.userCrypto.length - 1) return;
    const temp = this.userCrypto[this.selectedRow];
    this.userCrypto[this.selectedRow] = this.userCrypto[this.selectedRow + 1];
    this.userCrypto[this.selectedRow + 1] = temp;
    this.selectedRow = this.selectedRow + 1;
    this.storageService.saveToStorage(this.userCrypto);
  }

  editButton() {
    if (this.selectedRow === undefined) return; 
    const selectedCrypto = this.userCrypto[this.selectedRow];
    this.cryptoIdName.setValue(selectedCrypto.symbol);
    this.cryptoAmount.setValue(selectedCrypto.amount);
    this.cryptoLabel.setValue(selectedCrypto.label);
    this.getInputData(this.cryptoIdName.value);
    this.editCryptoOn = true;
  }

  deleteButton() {
    if (this.selectedRow === undefined) return; 
    this.userCrypto.splice(this.selectedRow, 1);
    this.deselectAll();
    this.storageService.saveToStorage(this.userCrypto);
  }
  reset() {
    this.resetCryptoIdName = false;
  }
}