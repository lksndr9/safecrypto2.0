/// <reference types="chrome"/>
import { Component, OnInit } from '@angular/core';
import { CryptoTablePopupModel } from 'src/app/models/crypto-table-popup.model';
import { UserCryptoModel } from 'src/app/models/user-crypto.model';
import { CryptoDataInterface } from 'src/app/components/interfaces/crypto-data.interface';
import { StorageInterface } from 'src/app/components/interfaces/storage.interface';
import { SimplePriceModel } from '../../models/simple-price-gecko.model';
import { TickerPrice } from 'src/app/models/provider-ticker.model';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {

  // List of user crypto currencies
  userCrypto: UserCryptoModel[] = [];
  // List of provider data for user crypto currencies                 
  providerCryptoData: SimplePriceModel[] = [];
  // Array of ready objects for table
  cryptoTablePopupObjects: CryptoTablePopupModel[] = [];
  sumOfAll: number = 0;

  constructor(
    private storageService: StorageInterface,
    private cryptoDataService: CryptoDataInterface,
    ) { }

  ngOnInit(): void {
    this.userCrypto = this.storageService.loadFromStorage();
    this.refreshPrices();
  }
  
  refreshPrices(): void {
    this.getTickerData();
    this.createPopupTable();
  }
  
  createPopupTable(): void {
    this.sumOfAll = 0;
    const tmp: CryptoTablePopupModel[] = []; 
    this.userCrypto.forEach(element => {  
      let providerData: SimplePriceModel | undefined = this.providerCryptoData.find(founded => founded.id === element.id);
      if (providerData === undefined) providerData = {id: '',  price: 0}
      let label =' - ' + element.label;
      if (element.label === undefined) label ='';
      const data: CryptoTablePopupModel = {
        id: element.symbol,
        name: element.name + label,
        logoUrl: element.thumb,
        // Limit price to 8 figures
        price: Math.round(providerData.price * 100000000) / 100000000,
        amount: element.amount,
        // Limit value to $ cents
        yourValue: Math.round(providerData.price * element.amount * 100) / 100
      }
      this.sumOfAll = this.sumOfAll + data.yourValue;
      tmp.push(data);
    });
    this.cryptoTablePopupObjects = tmp;
  }

  getTickerData(): void {
    this.providerCryptoData = [];
    this.cryptoDataService.getTicker(this.userCrypto).subscribe(
      data => {
        // Get keys from response
        let keys: string[] = Object.keys(data);
        let values: TickerPrice[] = Object.values(data);
        let i = 0;
          keys.forEach( key => {
            let newItem: SimplePriceModel = {
              id: key,
              price: values[i].usd
            }
            this.providerCryptoData.push(newItem);
            i++;
        });      
        this.createPopupTable();
      },
      error => {console.log(error)}
    );
  }
}