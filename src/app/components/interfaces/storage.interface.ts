import { Injectable } from "@angular/core";
import { UserCryptoModel } from "../../models/user-crypto.model";

// Data storage interface 

@Injectable({providedIn: 'root'})
export abstract class StorageInterface {
  abstract loadFromStorage(): UserCryptoModel[];
  abstract saveToStorage(data: UserCryptoModel[]) : void;
  abstract deleteStorage() : void;
}