import { Observable } from "rxjs";
import { UserCryptoModel } from "../../models/user-crypto.model";
import { ProviderGeckoAllModel } from '../../models/provider-gecko-all.model';

// Get crypto data from API 

export abstract class CryptoDataInterface {
  abstract getTicker(myCrypto: UserCryptoModel[]): Observable<any[]>;
  abstract getMetadata(cryptoName: string): Observable<ProviderGeckoAllModel>;
}