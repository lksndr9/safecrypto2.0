import { HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { toCamelCase } from '../utils/response.utils';

@Injectable({ providedIn: 'root' })
export class HttpResponseMapperInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.responseType === 'json') {
      return next.handle(req).pipe(map((response: any) => {
        if (response.body) {
          response.body = toCamelCase(response.body);
        }
        return response;
      }));
    } else {
      return next.handle(req);
    }
  }
}

export const HttpResponseMapperProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: HttpResponseMapperInterceptor,
  multi: true
};
