import { Injectable } from "@angular/core";
import { UserCryptoModel } from "../models/user-crypto.model";
import { StorageInterface } from "../components/interfaces/storage.interface";

@Injectable({providedIn: 'root'})
export class LocalStorageService implements StorageInterface {
  
  private key: string = 'user_data';
  
  loadFromStorage(): UserCryptoModel[] {
    const valueOfStorage: string | null = localStorage.getItem(this.key);
    return valueOfStorage === null ? [] : JSON.parse(valueOfStorage);
  }
  
  saveToStorage(data: UserCryptoModel[]): void {
    localStorage.setItem(this.key, JSON.stringify(data)); 
  }

  deleteStorage(): void {
    localStorage.removeItem(this.key);
  }
}
