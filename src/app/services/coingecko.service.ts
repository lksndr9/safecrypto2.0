import { Injectable } from '@angular/core';
import { CryptoDataInterface } from '../components/interfaces/crypto-data.interface';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { UserCryptoModel } from '../models/user-crypto.model';
import { catchError, delay, retryWhen, take } from 'rxjs/operators';
import { ProviderGeckoAllModel } from '../models/provider-gecko-all.model';

@Injectable({providedIn: 'root'})
export class CoinGeckoService implements CryptoDataInterface {

  private apiUrlTicker: string = 'https://api.coingecko.com/api/v3/simple/price?ids=';
  private apiUrlSearch: string = 'https://api.coingecko.com/api/v3/search?query=';

  constructor(private httpClient: HttpClient) {}
  
  public getTicker(myCrypto: UserCryptoModel[]): Observable<any> {
    // Preparing all Id's as strings separated by commas for request
    let ids: string = '';
    myCrypto.forEach(crypto => {
      ids += crypto.id + ',';
    });
    ids = ids.substring(0, ids.length - 1);

    // Making url for request
    return this.httpClient.get<any>(this.apiUrlTicker+ids+'&vs_currencies=usd')
    .pipe(
      retryWhen(
        errors => errors.pipe(
          delay(1000),
          take(10),
          catchError(error => {
            return throwError(error)
          })
        )
      )
    );
  }

  // Get property of specific asset
  public getMetadata(cryptoName: string): Observable<ProviderGeckoAllModel> {
    const url = this.apiUrlSearch + cryptoName;
    return this.httpClient.get<ProviderGeckoAllModel>(url)
    .pipe(
      retryWhen(
        errors => errors.pipe(
          delay(1000),
          take(10),
          catchError(error => {
            return throwError(error)
          })
        )
      )
    );
  }
}