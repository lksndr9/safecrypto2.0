export function toCamelCase(object: any): any {
  let result = object;
  if (typeof (object) === 'object') {
    if (object instanceof Array) {
      result = object.map(toCamelCase);
    } else {
      result = {};
      for (const key in object) {
        if (object.hasOwnProperty(key) && object[key] !== null) {
          const newKey = key.replace(/(_\w)/g, k => k[1].toUpperCase());
          result[newKey] = toCamelCase(object[key]);
        }
      }
    }
  }
  return result;
}
