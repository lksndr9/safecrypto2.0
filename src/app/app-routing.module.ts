import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HelpComponent } from './components/help/help.component';
import { OptionsComponent } from './components/options/options.component';
import { PopupComponent } from './components/popup/popup.component';


const routes: Routes = [
  {path: '', redirectTo: 'popup', pathMatch: 'full'},
  {path: 'popup', component: PopupComponent},
  {path: 'options', component: OptionsComponent},
  {path: 'help', component: HelpComponent},
  {path: '**', redirectTo: 'popup'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
